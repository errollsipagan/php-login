<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Method: GET");
header("Content-type: application/json");
require_once("check-login.php");

$mysqli = require __DIR__ . "/database.php";

$sql = "SELECT * FROM users";

$result = $mysqli->query($sql);

$users = $result->fetch_all();

echo json_encode($users);
exit();
?>