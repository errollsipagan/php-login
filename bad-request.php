<?php

function badRequest($message = "Bad Request") {
    header("Status: 400 Bad Request");
    die($message);
}

if (empty($_POST)) {
    badRequest();
}

?>