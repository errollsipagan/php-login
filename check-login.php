<?php
require_once("vendor/autoload.php");
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

$headers = getallheaders();
$authorization = "Authorization";

if (!array_key_exists($authorization, $headers)) {
    header("Status: 401 Unauthorized");
    exit();
}

$jwt = $headers[$authorization];

if (empty($jwt)) {
    header("Status: 401 Unauthorized");
    exit();
}

$key = getenv("JWT_KEY");
$algo = getenv("JWT_ALGO");

try {
    $decoded = JWT::decode($jwt, new Key($key, $algo));
} catch (Exception $e) {
    header("Status: 401 Unauthorized");
    exit();
}
?>