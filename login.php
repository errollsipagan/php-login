<?php
require_once("bad-request.php");
require_once("helpers.php");
require_once("vendor/autoload.php");
use Firebase\JWT\JWT;

$requiredFields = ["email", "password"];

foreach ($requiredFields as $value) {
    if (empty($_POST[$value])) {
        badRequest("{$value} is missing!");
    }
}

$mysqli = require __DIR__ . "/database.php";

$sql = sprintf("SELECT * FROM users 
                    WHERE email = '%s'",
                    $mysqli->real_escape_string($_POST["email"]));

$result = $mysqli->query($sql);

$user = $result->fetch_assoc();

if (!($user && password_verify($_POST["password"], $user["password_hash"]))) {
    header("Status: 401 Unauthorized");
    exit();
}

$payload = [
    "iss" => "localhost",
    "aud" => "localhost",
    "exp" => time() + 1000, //1min
    "data" => [
        "id" => $user["id"],
        "name" => $user["name"],
        "email" => $user["email"]
    ]
];

$jwt = JWT::encode($payload, getenv("JWT_KEY"), getenv("JWT_ALGO"));

header("Status: 200 Ok");
echo json_encode($jwt);
?>